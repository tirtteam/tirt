package com.example.chello.stormswarn;

public class StormResponseCopy {

//    private List<Storm> storm;

    private Storm storm;

//    public StormResponse(List<Storm> storm) {
//        this.storm = storm;
//    }
    public StormResponseCopy(Storm storm) {
    this.storm = storm;
}

//    public List<Storm> getStorm() {
//        return storm;
//    }

    public Storm getStorm() {
        return storm;
    }

//    public void setStorm(List<Storm> storm) {
//        this.storm = storm;
//    }

    public void setStorm(Storm storm) {
        this.storm = storm;
    }
}
