package com.example.chello.stormswarn;

import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by Chello on 2015-05-26.
 */
public class RestClient {
    private static RestApi REST_CLIENT;
//    private static String ROOT = "http://api.openweathermap.org/data/2.5";
    private static String ROOT = "http://192.168.1.6:8080";

    static {
        setupRestClient();
    }

    private RestClient() {}

    public static RestApi get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(ROOT)
                .setClient(new OkClient(new OkHttpClient()))
                .setLogLevel(RestAdapter.LogLevel.FULL);

        RestAdapter restAdapter = builder.build();
        REST_CLIENT = restAdapter.create(RestApi.class);
    }
}
