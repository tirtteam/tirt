package com.example.chello.stormswarn;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends ActionBarActivity implements LocationListener{

    static RestApi restApi = null;
    Button buttonSubmit;
    TextView textViewInMainActivity;
    private GoogleMap map = null;
    private LocationManager locationManager;
    private String bestProvider;
    final String gpsLocationProvider = LocationManager.GPS_PROVIDER;
    final String networkLocationProvider = LocationManager.NETWORK_PROVIDER;
    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;
    // The minimum distance to change Updates in meters
    final long MIN_DISTANCE = 10; // 10 meters
    // The minimum time between updates in milliseconds
    final long MIN_TIME = 1000 * 60 * 1; // 1 minute

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // check google services
        if (!isGooglePlayServicesAvailable()) finish();

        setMapIfNeeded();
        // Get the location manager
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (locationManager != null) {
            isGPSEnabled = locationManager.isProviderEnabled(gpsLocationProvider);
            isNetworkEnabled = locationManager.isProviderEnabled(networkLocationProvider);

            if (isGPSEnabled)
                locationManager.requestLocationUpdates(gpsLocationProvider,
                        MIN_TIME, MIN_DISTANCE, this);
            else if (isNetworkEnabled)
                locationManager.requestLocationUpdates(networkLocationProvider,
                        MIN_TIME, MIN_DISTANCE, this);
        }

        Criteria criteria = new Criteria();
        bestProvider = locationManager.getBestProvider(criteria, false);


        buttonSubmit = (Button)findViewById(R.id.button_submit);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String response = getWeather(v);
                String response = getStorm(v);
                Location location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                map.addMarker(new MarkerOptions()
                        .position(new LatLng(location.getLatitude(), location.getLongitude()))
                        .title("Heeeey!")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.clouds)));
                System.out.println(location.toString());
                Toast.makeText(getApplicationContext(), location.toString(), Toast.LENGTH_LONG);
//                getLocation();
            }
        });
    }

    /** Register for the updates when Activity is in foreground */
    @Override
    protected void onResume() {
        super.onResume();
        locationManager.requestLocationUpdates(bestProvider, 20000, 1, this);
    }

    /** Stop the updates when Activity is paused */
    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // This is the method that is called when the submit button is clicked
    String out = "";

    public String getStorm(View view) {
        RestClient.get().
                getStorm("Storm1", new Callback<StormResponse>() {
                    @Override
                    public void success(StormResponse stormResponse, Response response) {
                        System.out.println("REST: " + stormResponse.toString());
                        out.concat(stormResponse.getAzimuth());
                        // Add marker to map
                        double lat = Double.parseDouble(stormResponse.getLatitude());
                        double lon = Double.parseDouble(stormResponse.getLongtitude());
                        map.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.clouds))
                                .title(stormResponse.getId()));
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.i("RestClient", "Something went wrong: " + error.getResponse().getStatus());
                        Log.i("RestClient", "Something went wrong: " + error.getResponse().getBody());
                        Log.i("RestClient", "Something went wrong: " + error.getCause());
                    }
                });
        System.out.println("REST: " + "cosik?");
        return out;
    }

//    public String getStormList(View view) {
//        RestClient.get().
//                getStorm("Storm1", new Callback<StormResponse>() {
//                    @Override
//                    public void success(StormResponse stormResponse, Response response) {
//                        out.concat(stormResponse.getStorm().get(0).getAzimuth());
//                        // Add marker to map
//                        double lat = Double.parseDouble(stormResponse.getStorm().get(0).getLat());
//                        double lon = Double.parseDouble(stormResponse.getStorm().get(0).getLongt());
//                        map.addMarker(new MarkerOptions()
//                                .position(new LatLng(lat, lon))
//                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.clouds))
//                                .title(stormResponse.getStorm().get(0).getId()));
//                    }
//
//                    @Override
//                    public void failure(RetrofitError error) {
//                        Log.i("RestClient", "Something went wrong: " + error.getResponse().getStatus());
//                        Log.i("RestClient", "Something went wrong: " + error.getResponse().getBody());
//                        Log.i("RestClient", "Something went wrong: " + error.getCause());
//                    }
//                });
//        System.out.println("REST: " + "cosik?");
//        return out;
//    }

    public String getWeather(View view) {

        RestClient.get().getWeather("California", new Callback<WeatherResponse>() {
            @Override
            public void success(WeatherResponse weatherResponse, Response response) {
                out.concat(weatherResponse.getBase());
                out += weatherResponse.getWeather().get(0).getMain();
                out += weatherResponse.getWeather().get(0).getDescription();
                System.out.println("AAAA: " + weatherResponse.getCoord());
                // Add marker to map
                map.addMarker(new MarkerOptions()
                    .position(new LatLng(weatherResponse.getCoord().getLat(), weatherResponse.getCoord().getLon()))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.clouds))
                    .title(weatherResponse.getWeather().get(0).getDescription()));
            }

            @Override
            public void failure(RetrofitError error) {
                Log.i("RestClient", "Something went wrong: " + error.getResponse().getStatus());
                Log.i("RestClient", "Something went wrong: " + error.getResponse().getBody());
                Log.i("RestClient", "Something went wrong: " + error.getCause());
            }
        });
        return out;
    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (status == ConnectionResult.SUCCESS) return true;
        else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0);
            return false;
        }
    }

//    protected synchronized void buildGoogleApiClient() {
//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this)
//                .addApi(LocationServices.API)
//                .build();
//    }

    public void setMapIfNeeded() {
        if (map == null) {
            map = ((MapFragment)getFragmentManager().findFragmentById(R.id.map)).getMap();
            map.setMyLocationEnabled(true);
        }
    }

    public Location getLocation() {
        LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);


        if (!isGPSEnabled && !isNetworkEnabled) finish();
        else {
            Location lastKnownLocationByGPS = lm.getLastKnownLocation(gpsLocationProvider);
            Location lastKnownLocationNetwork = lm.getLastKnownLocation(networkLocationProvider);

            if (lastKnownLocationByGPS != null)
                Toast.makeText(getApplicationContext(), lastKnownLocationByGPS.toString(), Toast.LENGTH_LONG);
            if (lastKnownLocationNetwork != null)
                Toast.makeText(getApplicationContext(), lastKnownLocationNetwork.toString(), Toast.LENGTH_LONG);

            if (isNetworkEnabled) {
                lm.requestLocationUpdates(networkLocationProvider, MIN_TIME, MIN_DISTANCE, this);
            }
            return lastKnownLocationNetwork;
        }
        return null;
    }

    private void addMyLocationToMap() {
        if (map != null) {
            LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    map.addMarker(new MarkerOptions()
                            .position(new LatLng(location.getLatitude(), location.getLongitude()))
                            .title("Me")
                            .snippet("There I am!"));
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            });
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
