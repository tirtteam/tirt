package org.gradle;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.tomcat.jni.Sockaddr;

public class StormServer implements Callable{
	
	private ServerSocket serverSocket;
	private int port;
	private String output;
	
	public StormServer (int port) throws IOException {
		this.port = port;
		this.output = "";
	}
	
	public String call() {
		try {
			serverSocket = new ServerSocket(port);
			serverSocket.setSoTimeout(1000*60);
//				serverSocket.setSoTimeout(0);
			
			System.out.println("Waiting for clients on port: " + 
			serverSocket.getLocalPort() + "...");
			
			Socket server = serverSocket.accept();
			System.out.println("Conencted to: " + server.getRemoteSocketAddress());
		
//				DataInputStream in = new DataInputStream(server.getInputStream());
//				System.out.println(in.readUTF());
			
//				DataInputStream in = new DataInputStream(server.getInputStream());
			BufferedReader in = new BufferedReader(new InputStreamReader(server.getInputStream()));

//				String output = new String(in.readUTF());
			output = new String(in.readLine());
			System.out.println(output);
			
	        server.close();
		}
		catch(SocketTimeoutException s) { System.out.println("Socket timed out!"); }
		catch(IOException e) { e.printStackTrace(); }

		return output;
	}
	
	   public static void main(String [] args) {
	      int port = 40020;

	      try {
		      ExecutorService pool = Executors.newFixedThreadPool(3);
		      Callable<String> callable = new StormServer(port);
		      Future<String> future = pool.submit(callable);
	//	      Set<Future<String>> set = new HashSet<Future<Integer>>();
	//	      for (String word: args) {
	//	        Callable<Integer> callable = new WordLengthCallable(word);
	//	        Future<Integer> future = pool.submit(callable);
	//	        set.add(future);
	//	      }
		      String ouString = future.get();
			  System.out.println("The response is: " + ouString);
			} catch (InterruptedException e) {
				System.out.println("Something was interrupted!");
				e.printStackTrace();
			} catch (ExecutionException e) {
				System.out.println("Bad execution!");
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
	      System.out.println("Program ended!");
	      System.exit(0);
	    }	
}
